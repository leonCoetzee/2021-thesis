\babel@toc {UKenglish}{}\relax 
\contentsline {section}{\numberline {1}Abstract}{1}{section.1}%
\contentsline {section}{\numberline {2}Introduction}{3}{section.2}%
\contentsline {section}{\numberline {3}Literature Review}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Understanding and creating Form}{8}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Computers in Architectural design}{11}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Architectural Form-Finding (beginnings and theory)}{18}{subsection.3.3}%
\contentsline {subsection}{\numberline {3.4}Architectural Solar controls - shading devices (Sun-Shades)}{25}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Research on daylighting, solar radiation, energy efficient buildings and form-finding}{28}{subsection.3.5}%
\contentsline {subsubsection}{\numberline {3.5.1}Fa\c {c}ade design optimisation for daylight with a simple genetic algorithm}{28}{subsubsection.3.5.1}%
\contentsline {subsubsection}{\numberline {3.5.2}Genetic optimization of external shading devices}{29}{subsubsection.3.5.2}%
\contentsline {subsubsection}{\numberline {3.5.3}Geometric optimization of fenestration}{31}{subsubsection.3.5.3}%
\contentsline {subsubsection}{\numberline {3.5.4}Optimal Building Envelope Design - History, current status, new trends}{32}{subsubsection.3.5.4}%
\contentsline {subsubsection}{\numberline {3.5.5}Shape optimization of free-form buildings}{33}{subsubsection.3.5.5}%
\contentsline {subsubsection}{\numberline {3.5.6}Robotic Form-Finding and Construction}{36}{subsubsection.3.5.6}%
\contentsline {subsubsection}{\numberline {3.5.7}Comments regarding the Research in daylighting, solar radiation, building envelopes}{38}{subsubsection.3.5.7}%
\contentsline {subsection}{\numberline {3.6}General Comments}{40}{subsection.3.6}%
\contentsline {section}{\numberline {4}Methods}{41}{section.4}%
\contentsline {subsection}{\numberline {4.1}Introduction}{41}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Background}{42}{subsection.4.2}%
\contentsline {subsubsection}{\numberline {4.2.1}Solar Angles}{43}{subsubsection.4.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2}Solar values as an example}{45}{subsubsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.3}Digital Solar Protractor}{48}{subsubsection.4.2.3}%
\contentsline {subsubsection}{\numberline {4.2.4}The window opening and `bounding box'}{50}{subsubsection.4.2.4}%
\contentsline {subsubsection}{\numberline {4.2.5}Calculating sun rays}{50}{subsubsection.4.2.5}%
\contentsline {subsection}{\numberline {4.3}Evolutionary Algorithms}{55}{subsection.4.3}%
\contentsline {subsubsection}{\numberline {4.3.1}General Outline}{56}{subsubsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.2}Why Evolution Strategies for the algorithm?}{56}{subsubsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.3}Fitness}{57}{subsubsection.4.3.3}%
\contentsline {subsubsection}{\numberline {4.3.4}Building fa\c {c}ade}{57}{subsubsection.4.3.4}%
\contentsline {subsubsection}{\numberline {4.3.5}Bounding box - point cloud}{58}{subsubsection.4.3.5}%
\contentsline {subsubsection}{\numberline {4.3.6}Population}{58}{subsubsection.4.3.6}%
\contentsline {subsubsection}{\numberline {4.3.7}Recombination}{58}{subsubsection.4.3.7}%
\contentsline {subsubsection}{\numberline {4.3.8}Mutation}{58}{subsubsection.4.3.8}%
\contentsline {subsubsection}{\numberline {4.3.9}Summary of ES}{59}{subsubsection.4.3.9}%
\contentsline {subsection}{\numberline {4.4}Pseudocode - Evolution Strategies}{60}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Point cloud to mesh}{61}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Traditional Sun-Shade `fitness'}{62}{subsection.4.6}%
\contentsline {section}{\numberline {5}Experiments and Results}{64}{section.5}%
\contentsline {subsection}{\numberline {5.1}University of Cape Town - Data Management}{64}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Traditional Sun-shades Fitness results}{64}{subsection.5.2}%
\contentsline {subsubsection}{\numberline {5.2.1}Traditional Sun-shades Fitness - comments}{66}{subsubsection.5.2.1}%
\contentsline {subsection}{\numberline {5.3}Evolved Sun-shades mesh results}{66}{subsection.5.3}%
\contentsline {subsubsection}{\numberline {5.3.1}Evolved Sun-shades meshes - comments}{69}{subsubsection.5.3.1}%
\contentsline {subsection}{\numberline {5.4}Evolution Strategies - fitness results}{70}{subsection.5.4}%
\contentsline {subsubsection}{\numberline {5.4.1}Individual Fitness values}{70}{subsubsection.5.4.1}%
\contentsline {subsubsection}{\numberline {5.4.2}Fitness values per Generation}{71}{subsubsection.5.4.2}%
\contentsline {subsubsection}{\numberline {5.4.3}Mean fitness per Evolution Strategies run compared to traditional sun-shade}{71}{subsubsection.5.4.3}%
\contentsline {subsection}{\numberline {5.5}Hypothesis testing - single sample T test (two tailed)}{76}{subsection.5.5}%
\contentsline {subsubsection}{\numberline {5.5.1}Comments and observations}{77}{subsubsection.5.5.1}%
\contentsline {section}{\numberline {6}Discussion}{78}{section.6}%
\contentsline {subsection}{\numberline {6.1}Form-Finding}{78}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Evolution Strategies}{79}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}General observations}{79}{subsection.6.3}%
\contentsline {section}{\numberline {7}Conclusion and Future Work}{81}{section.7}%
\contentsline {subsection}{\numberline {7.1}Conclusion}{81}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Future Work}{81}{subsection.7.2}%
\contentsline {subsubsection}{\numberline {7.2.1}Explore other fa\c {c}ade conditions}{81}{subsubsection.7.2.1}%
\contentsline {subsubsection}{\numberline {7.2.2}Changes to the Evolutionary Algorithm}{81}{subsubsection.7.2.2}%
\contentsline {subsubsection}{\numberline {7.2.3}Add simulation to improve upon the idea of fitness}{82}{subsubsection.7.2.3}%
\contentsline {subsubsection}{\numberline {7.2.4}Reconnecting to `original' Form-finding principals}{83}{subsubsection.7.2.4}%
\contentsline {subsubsection}{\numberline {7.2.5}Meshing and surfacing of the point cloud}{83}{subsubsection.7.2.5}%
\contentsline {section}{References}{85}{section*.59}%
\contentsline {section}{\numberline {8}Appendix 1 - Source Code}{90}{section.8}%
\contentsline {section}{\numberline {9}Appendix 2 - Source Code cleaning output}{101}{section.9}%
\contentsline {section}{\numberline {10}Appendix 3 - Solar calculations (every 15s)}{103}{section.10}%
\contentsline {section}{\numberline {11}Appendix 4 - Individual fitness details}{109}{section.11}%
\contentsline {section}{\numberline {12}Appendix 5 - Fitness values per generation per ES run}{111}{section.12}%
\contentsline {section}{\numberline {13}Appendix 6 - Fitness per fa\c {c}ade}{115}{section.13}%
