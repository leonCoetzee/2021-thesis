\clearpage\section{Introduction}
% WORD COUNT: 1266 (incl footnotes)
% 2-3 pages at most
\begin{comment}
\end{comment}

%\subsection{Background Information}
This case study explores using an Evolutionary Algorithm to design an optimum sun-shading device for application in Architecture. Sun-shading devices are external elements on a building fa\c{c}ade that typically shield a window opening with the intention of blocking direct sunlight from entering and warming the interior. To assist in keeping the work focused on evolutionary computation, the scope of the study shall be limited to only consider the successful reduction of solar radiation on a surface. This research does not consider light quality or comfort levels (glare) for building occupants. Over time, various approaches and solutions have been shown to produce effective devices for certain conditions. Both how to approach this design task (with assistance on how to analyse the conditions under which such a device should function) and how to select a solution from an existing set of `typical' sun-shades have been well documented and are typically used as architectural `precedent studies' when determining a solution. This case study sees these solutions as `traditional' sun-shades, examples of which can be seen in Figure \ref{fig:TipsDaylighting1}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{Image01-tipsForSunlight.png}
  \caption{Traditional sun-shades proposed in `Tips for Daylighting with Windows' (O'Conner et al.)}
  \label{fig:TipsDaylighting1}
\end{figure}

Current research of a similar scope as that carried out in this case study considers, among others; fa\c{c}ade design optimisation, the optimisation of a given (pre-designed) shading device, the geometric optimisation of fenestration (window layout) within a fa\c{c}ade, the optimal design of a building envelope\footnote{The Building Envelope can be seen as the element that separates the interior from the exterior of a building. The envelopes' ability to function as a barrier to better manage the internal climate of a building is what is of interest.}, the optimisation of the shape of a free-form building and the optimised design of an energy efficient building fa\c{c}ade.

\textbf{Fa\c{c}ade design optimisation}: The optimisation of a fa\c{c}ade design seeks to balance allowing natural daylight to enter the building (to provide adequate internal lighting) while also minimising the resultant glare which would cause discomfort for the users. This research is based around creating a natural daylight system with the hope that this can reduce the energy costs of the building. The optimisation solution is centred on the design of a window with integrated sun-shades and uses a simulation of sky brightness to determine internal luminance values.

\textbf{Optimisation of a (given) shading device}: The optimisation of a given sun-shading device looks to address the increase in heat build up within a building while being restricted in following existing building regulation guidelines that concern the placement together with a broad outline of a device but is lacking in details concerning the choice of sun-shades that may be used. This research does not alter the form of the device, instead, using the results from an energy simulation based on readings from within a test space, the existing device is `optimised' by altering the position of components (relative to the building surface) - how the device may be rotated and by how wide or thick the components may be (this needs to fit within the building regulations).

\textbf{Geometric optimisation of fenestration}: The geometric optimisation of windows on a fa\c{c}ade is inspired by the fenestration of Notre Dame du Haut - Ronchamp Chapel by Le Corbusier. The aim of this study being to reduce the size of window openings on a fa\c{c}ade to minimise heat build up within. This work looks at optimising the design of a window layout by affecting the shape, amount and position of window openings. The fa\c{c}ade is divided into cells which drive the optimisation of openings, however, certain constraints are implemented (window opening aspect ratios and amounts) to allow the designer to exercise control over the solution. Examples of fa\c{c}ade and sun-shading device optimisation approaches can be seen in Figure \ref{fig:ExampleSunShadeOpt1}.

% https://tex.stackexchange.com/questions/35125/how-to-use-the-placement-options-t-h-with-figures
% {figure} [h] h - here, t - top, b - bottom, p - page, ! - ignore (certain parameters) 
\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{torres_sakamoto.png}
    \caption{Fa\c{c}ade design optimisation for daylight (Torres \& Sakamoto)}
    \label{fig:TorresSakamoto1}
    \end{subfigure}\hspace{5mm}% https://tex.stackexchange.com/questions/332572/how-do-i-add-additional-horizontal-spacing-between-two-figures-in-latex
  %
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{manzan_pinto.png}
    \caption{External shading device optimisation (Manzan \& Pinto)}
    \label{fig:ManzanPinto1}
  \end{subfigure}
  \caption{Optimisation of sun-shading fa\c{c}ade and devices}
  \label{fig:ExampleSunShadeOpt1}
\end{figure}

\textbf{Optimal design of a building envelope}: Optimum building envelope design is broader in scale - the building as a whole is considered within an environment. Typically software simulation is used to mimic the performance of a building, the results of which can be used to create a `Green Building' (a high performance, energy efficient, low environmental impact building). Given both the scale of the work (the entire building) and the many variables to be considered in the optimisation solution, this research tends to be rich and complex.

\textbf{Optimisation of a free-form building shape}: Research on the shape optimisation of free-form buildings hopes to increase solar radiation within the building, set in a cold climate, by optimising the shape of a free-form building where solar radiation, the `shape coefficient' and `space efficiency' are considered. Parametric\footnote{Parametric refers to a software modelling method that allows the user to access parameters that make up the model. This is often `dynamic' as changes to parameters are immediately reflected in the model.} modelling creates the free-form building model while a genetic algorithm is used to maximise solar radiation gain and space efficiency while minimising the shape coefficient\footnote{The shape coefficient is the ratio between the area of a building's external surface and its internal volume.} this can be visualised in Figure \ref{fig:BuildingEnvShpCoEff1}.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{Zhang shape coefficient.PNG}
  \caption{Optimum building envelope design - shape coefficient visualisation (Zhang et al.)}
  \label{fig:BuildingEnvShpCoEff1}
\end{figure}

%\subsection{Research questions}
This case study - Evolution of Sun-Shades Outside Building Fa\c{c}ades - is unique in that it considers the form the final, optimised sun-shade will take. Architecturally, this is known as `form-finding' and it is a valid design strategy employed by the Architect (or designer) in a scenario where they cannot anticipate what shape the final product will take when solving a given problem. \textbf{Can the evolved form of a sun-shade perform better than existing `traditional' sun-shades?} The Form-finding in this case study is more mature when compared to other research in that other sun-shade optimisation research typically modifies existing elements or alters a given layout by keeping parts constrained and moving components - lengthening, shortening or rotating as needed. Whereas this case study starts with a point cloud and evolves this volume to a final, unknown, form. Furthermore, what makes this research unique is that it determines a method with which to evaluate the fitness of traditional sun-shades and then uses this fitness value to compare traditional sun-shades with the newer geometrically evolved sun-shade. Finally, the Evolutionary Algorithm, in this case study, is permitted to function fully; no `designer' intervention is made, no parameter tweaks are permitted and the algorithm functions fully until the final generation is reached (no population `restart' is required). This research uses a `pure' Evolutionary Algorithm, not an implemented `plug-in' or software generated solution. No limit is set on the evolutionary process as it functions separately from an environmental simulation which typically require simplified or pre-calculated data (often contained within a database).

Regarding `Form-finding'; this approach to architectural design has a strong heritage going back to Antoni Gaud{\'i}'s `funicular'\footnote{The funicular was a series of chains suspended from the ceiling. The form the chains took on visually illustrate the structural forces that would be experienced in his chapel design (shown upside down). The structure can be designed from this knowing it can accommodate the distribution of forces.} and being aptly demonstrated by Frei Otto's `Soap Bubble'\footnote{Frei Otto coined the phrase `Form-finding', which he used to describe his method of using soap bubbles to determine the shape the tensile roof would take given the structure and supports he designed for the 1972 Olympic Stadium in Munich - Olympiastadion.} experiments. Figure \ref{fig:FreeForm1} illustrates these original methods of Form-finding.

\begin{figure}[H]
\centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{Gaudi-funicular-Martinell.png}
    \caption{Antoni Gaud{\'i} Funicular for the Col{\`o}nia G{\"u}ell chapel (Martinell)}
    \label{fig:GaudiFunicular1}
    \end{subfigure}\hspace{5mm}
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{Frei-Otto-Soap-Bubbles.png}
    \caption{Frei Otto Experimenting with Soap Bubbles (Zexin)}
    \label{fig:FreiOtto1}
  \end{subfigure}
    \caption{Architectural Form-finding: Antoni Gaud{\'i} and Frei Otto}
    \label{fig:FreeForm1}
\end{figure}

%\subsection{Summary}
While Evolutionary Algorithms are perfectly suited to the `form-finding' design method - where they evolve a solution to suit a set of given fitness parameters with the resulting artefact being unknown - in architectural research they are typically used to optimise a series of given parameters. However, these results see a general `re-arranging of existing elements' rather than the creation of something new and possibly unexpected.